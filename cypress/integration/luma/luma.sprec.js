/// <reference types="cypress"/>

import loc from '../../support/luma/locatorsLogin'
import '../../support/luma/commands.Login'
import Jacket from '../../support/luma/PageObject/Jacket/index'
import Pants from '../../support/luma/PageObject/Pants'
import Shorts from '../../support/luma/PageObject/Shorts'
import Cart from '../../support/luma/PageObject/Cart'



describe ('Should test', ()=> {
        before(() => {

           // cy.visit('https://magento2-demo.magebit.com/')
        
             })
             beforeEach(() => {
                cy.visit('https://magento2-demo.magebit.com/')
                
             })
        
        it('Create account, SUCCESS', () => {
            cy.create('roni','cost','roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.ACCOUNT_MENSAGEM.SUCCESS).should('contain','Thank you for registering with Main Website Store.')
        })

        it('Create account, already existing ', () => {
            cy.create('roni','cost','roni_cost@example.com','roni_cost3@example.com','roni_cost3@example.com')
            cy.get(loc.ACCOUNT_MENSAGEM.MESSAGE_ERROR).should('contain','There is already an account with this email address.')
        })

        it('Create account, confirmaton password', () => {
            cy.create('vagner','rosales','vagner@hotmail.com','123456','12345')
            cy.get(loc.ACCOUNT_MENSAGEM.CONFIRMATION_PASSWORD_ERROR).should('contain','Please enter the same value again.')
            
        })

        it('Create account, Weak passsord', () => {
            cy.create('vagner','rosales','vagner@hotmail.com','123','123')
            cy.get(loc.ACCOUNT_MENSAGEM.PASSWORD_ERROR).should('contain','Minimum length of this field must be equal or greater than 8 symbols. Leading and trailing spaces will be ignored.')
           
        })
      
        it('Create account, empty', () => {
         
            cy.create('  ','  ','  ','  ',' ')
            cy.get(loc.ACCOUNT_MENSAGEM.FIRST_NAME_ERROR).should('contain','This is a required field.')
            cy.get(loc.ACCOUNT_MENSAGEM.LAST_NAME_ERROR).should('contain','This is a required field.')
            cy.get(loc.ACCOUNT_MENSAGEM.EMAIL_ERROR).should('contain','This is a required field.')
            cy.get(loc.ACCOUNT_MENSAGEM.PASSWORD_ERROR).should('contain','This is a required field.') 
            cy.get(loc.ACCOUNT_MENSAGEM.CONFIRMATION_PASSWORD_ERROR).should('contain','This is a required field.')
            
        })

        it('Login Sucesso ', () => {
            cy.login('roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.LOGIN_SUCESSO).should('contain', 'Welcome') 
         
        })
    
        it('Login Erro EMAIL ', () => {
            cy.login(' @   @example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.EMAIL_ERRO).should('contain','Please enter a valid email address (Ex: johndoe@domain.com).')
        })

        it('Login Erro PASSWORD ', () => {
            cy.login('roni_cost@example.com','ron')
            cy.get(loc.LOGIN_MENSAGEM.PASSWORD_ERROR).should('contain','The account sign-in was incorrect')
             
        })

        it('add Jacket, caminho alternativo SALE', () => {
            cy.login('roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.LOGIN_SUCESSO).should('contain', 'Welcome') 
            Jacket.access_jacket()
            Jacket.add_jacket()
        })
        it('add Pants, caminho alternativo HOME', () => {
            cy.login('roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.LOGIN_SUCESSO).should('contain', 'Welcome') 
            Pants.access_Pants()
            Pants.add_pants()
              
        })
        it('add WhatsNew, caminho alternativo WHATSNEW', () => {
            cy.login('roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.LOGIN_SUCESSO).should('contain', 'Welcome')
            Shorts.access_shorts()
            Shorts.add_shorts()
                   
        })
     
        it('cart', () => {
            cy.login('roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.LOGIN_SUCESSO).should('contain', 'Welcome')
            Jacket.access_jacket()
            Jacket.add_jacket()
            Cart.access_cart()
            Cart.cart()
         }) 
        it.only('cart vazio', () => {
            cy.login('roni_cost@example.com','roni_cost3@example.com')
            cy.get(loc.LOGIN_MENSAGEM.LOGIN_SUCESSO).should('contain', 'Welcome')
            Cart.access_cart()
            Cart.cart_mesage()
        }) 
    
    })     

//  Realize implementação dos cenários de testes automatizados, fazendo a utilização de boas práticas de código da forma mais organizada que conseguir para os seguintes fluxos:
//  Cadastro de usuário 
//  Login
//  Adicionar produto ao carrinho
//  Finalização de compra
 
//  Realize a documentação de instalação e execução de forma clara, considerando a possibilidade de alguma pessoa iniciante dar continuidade em seu projeto .
//  Cenário opcionais:
    // Esqueci minha senha
    // Adicionar produto ao carrinho em fluxos alternativos:
    // Página de produto
    // Busca
    // Validação do pedido realizado
    // Lista de pedido
    // Detalhe do pedido