export const ELEMENTS  = {

    HOME:       '.home > a',
    SEARCH:     '#search',

    MENU:{
        HOME:            '.home-pants > img',
        WHATSNEW:        '#ui-id-3 > span',
    },
    
    CART:                       '.showcart',
    TOP_BTN_CHECKOUT:           '#top-cart-btn-checkout',
    PROCEED_CHECKOUT:           'tbody > :nth-child(1) > .col-price',
    BTN_NEXT:                   '.button',
    PAYMENT_METHOD:             '.payment-method-content > :nth-child(4) > div.primary > .action > span',
    CHECKOUT_SUCESS:            '.checkout-success > :nth-child(1)',
    CHECKOUT_BACK:              '.checkout-success > .actions-toolbar > div.primary > .action > span',
    MESAGE:                     '.subtitle',
    DELETE:                     '.product-item-details > .actions > .secondary > .action',
    CONFIRMAR_OK:                '.action-primary > span',
}
       