import loc from './elements'
// selecionar size
// selecionar color
// add no carrinho

const elem = require('./elements').ELEMENTS;
class Cart{

    access_cart(){
        cy.get(elem.CART).click()

    }
    cart(){
        cy.wait(3000)
        cy.get(elem.TOP_BTN_CHECKOUT).click({force: true})
        cy.wait(2000)
        cy.get(elem.PROCEED_CHECKOUT).click({force: true})
        cy.get(elem.BTN_NEXT).click()
        cy.get(elem.PAYMENT_METHOD).click({force: true})
        cy.get(elem.CHECKOUT_SUCESS).should('contain','Your order number is: ')
        cy.wait(1000)
        cy.get(elem.CHECKOUT_BACK).click()
    }
    cart_mesage(){
        cy.get(elem.MESAGE).should('contain','You have no items in your shopping cart.')
    }
    cart_delete(){
        cy.get(elem.DELETE).click()
        cy.get(elem.CONFIRMAR_OK).click()
    }
}

export default new Cart();