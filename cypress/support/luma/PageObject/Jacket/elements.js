export const ELEMENTS  = {

    SIZE:                       '#option-label-size-157-item-170',
    COLOR:                      '#option-label-color-93-item-49',
    ADD:                        '#product-addtocart-button',
    EXIST:                      '.fieldset > .actions',
    MESAGE:                     '.message-success > div',

    HOME:     '.home > a',

    MENU:{
        SALE:                   '#ui-id-8 > span',  
    },      
    SALE:{
        JACKET:                 '.categories-menu > :nth-child(2) > :nth-child(2) > a',
        TEES:                   '.categories-menu > :nth-child(2) > :nth-child(3) > a',
    },
    JACKET:{
        LINK_JACKET:            '.name > .product-item-link',
    },
   
}
