import loc from './elements'
// selecionar size
// selecionar color
// add no carrinho

const elem = require('./elements').ELEMENTS;
class Jacket{

    access_jacket(){
        cy.get(elem.MENU.SALE).click()
        cy.get(elem.SALE.JACKET).click()
        cy.get(elem.JACKET.LINK_JACKET).click()

    }

    add_jacket(){
        cy.get(elem.SIZE).click()
        cy.get(elem.COLOR).click()
        cy.get(elem.ADD).click()
        cy.get(elem.EXIST).should('exist')
        cy.get(elem.MESAGE).should('contain','You added ')
        cy.get(elem.HOME).click()


          
    }
}

export default new Jacket();