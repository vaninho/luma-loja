export const ELEMENTS  = {

    SIZE:                '#option-label-size-157-item-181',
    COLOR:               '#option-label-color-93-item-53',
    ADD:                 '#product-addtocart-button',
    EXIST:               '.fieldset > .actions',
    MESAGE:              '.message-success > div',

    HOM:                '.home > a',

    MENU:{
        HOME:            '.home-pants > .content',
    },
    HOME:{
        LINK_PANTS:       '#product-item-info_771 > .details > .name > .product-item-link',
    },
}
