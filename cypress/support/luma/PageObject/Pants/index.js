import loc from './elements'
// selecionar size
// selecionar color
// add no carrinho

const elem = require('./elements').ELEMENTS;
class Pants{

    access_Pants(){
        cy.get(elem.MENU.HOME).click({force: true})
        cy.get(elem.HOME.LINK_PANTS).click({force: true})

    }

    add_pants(){
        cy.get(elem.SIZE).click()
        cy.get(elem.COLOR).click()
        cy.get(elem.ADD).click()
        cy.get(elem.EXIST).should('exist')
        cy.get(elem.MESAGE).should('contain','You added ') 
        cy.get(elem.HOM).click()

    }
}

export default new Pants();