export const ELEMENTS  = {

    SIZE:                '#option-label-size-157-item-175',
    COLOR:               '#option-label-color-93-item-56',
    ADD:                 '#product-addtocart-button',
    EXIST:               '.fieldset > .actions',
    MESAGE:              '.message-success > div',
   
    HOME:       '.home > a',
    SEARCH:     '#search',

    MENU:{
        HOME:            '.home-pants > img',
        WHATSNEW:        '#ui-id-3 > span', 

    },
    WHATSNEW:{
        SHORTS:         ':nth-child(2) > :nth-child(6) > a',
        },
    SHORTS:{
        LINK_SHORTS:    '#product-item-info_1959 > .details > .name > .product-item-link',
    },

}
