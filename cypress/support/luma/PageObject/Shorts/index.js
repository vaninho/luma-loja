import loc from './elements'
// selecionar size
// selecionar color
// add no carrinho

const elem = require('./elements').ELEMENTS;
class Shorts{

    access_shorts(){
        cy.get(elem.MENU.WHATSNEW).click()
        cy.get(elem.WHATSNEW.SHORTS).click()
        cy.get(elem.SHORTS.LINK_SHORTS).click()

    }
    add_shorts(){
        cy.get(elem.SIZE).click()
        cy.get(elem.COLOR).click()
        cy.get(elem.ADD).click()
        cy.get(elem.EXIST).should('exist')
        cy.get(elem.MESAGE).should('contain','You added ')  
        cy.get(elem.HOME).click()
    }
}

export default new Shorts();