
import loc from './locatorsLogin'

Cypress.Commands.add('login',(user, passowd)=> {
    cy.visit('https://magento2-demo.magebit.com/') 
    cy.get('.panel > .header > .authorization-link > a').click() 
    cy.get(loc.LOGIN.USER).type(user)
    cy.get(loc.LOGIN.PASSWORD).type(passowd)
    cy.get(loc.LOGIN.BTN_LOGIN).click()
   
 })

  Cypress.Commands.add('create',(first_name, last_name,email,password,confirmation_password)=> {
    cy.visit('https://magento2-demo.magebit.com/') 
    cy.get('body > div:nth-child(8) > header:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)').click()
    cy.get(loc.ACCOUNT.FIRST_NAME).type(first_name)
    cy.get(loc.ACCOUNT.LAST_NAME).type(last_name)
    cy.get(loc.ACCOUNT.EMAIL).type(email)
    cy.get(loc.ACCOUNT.PASSWORD).type(password)
    cy.get(loc.ACCOUNT.CONFIRMATION_PASSWORD).type(confirmation_password)
    cy.get(loc.ACCOUNT.BTN_CREATE).click()
})



