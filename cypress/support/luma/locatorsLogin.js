const locatorsLogin ={
    LOGIN:{
        USER:             '#email',
        PASSWORD:         '.login-container > .block-customer-login > .block-content > #login-form > .fieldset > .password > .control > #pass',
        BTN_LOGIN:        '.login-container > .block-customer-login > .block-content > #login-form > .fieldset > .actions-toolbar > div.primary > #send2',
    },
    LOGIN_MENSAGEM:{
        LOGIN_SUCESSO:  ':nth-child(2) > .greet > .logged-in',
        EMAIL_ERRO:     '#email-error', 
        PASSWORD_ERROR:  '.message-error > div ',          
    },
    ACCOUNT:{
        FIRST_NAME:             '#firstname',
        LAST_NAME:              '#lastname',
        EMAIL:                  '#email_address',
        PASSWORD:               '#password',
        CONFIRMATION_PASSWORD:  '#password-confirmation',
        BTN_CREATE:             '#form-validate > .actions-toolbar > div.primary > .action',  
    },
    ACCOUNT_MENSAGEM:{
       
        FIRST_NAME_ERROR:                    '#firstname-error',   
        LAST_NAME_ERROR:                     '#lastname-error',
        EMAIL_ERROR:                         '#email_address-error',
        PASSWORD_ERROR:                      '#password-error',
        CONFIRMATION_PASSWORD_ERROR:         '#password-confirmation-error',
        SUCCESS:                             '.message-success > div',
        MESSAGE_ERROR:                       '.message-error > div',
          
    },
    MENU:{
        HOME:                   '.home-pants > img',
        WHATSNEW:               '#ui-id-3 > span',
        WOMEN:                  '#ui-id-4 > :nth-child(2 )',
        MEN:                    '#ui-id-5 > :nth-child(2)',
        GEAR:                   '#ui-id-6 > :nth-child(2)',
        TRAINING:               '#ui-id-7 > :nth-child(2)', 
        SALE:                   '#ui-id-8 > span',         
        GIFTCARD:               '#ui-id-6 > :nth-child(2)', 
    },
}
export default locatorsLogin;